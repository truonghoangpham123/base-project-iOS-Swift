//
//  BaseTableViewCell.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/7/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureCell<T>(anyItem: T) {
        
    }
}

@objc protocol TableViewCellDelegate: class {
    @objc optional func selected(indexPath: IndexPath)
}
