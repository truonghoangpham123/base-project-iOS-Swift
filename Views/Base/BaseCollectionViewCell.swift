//
//  BaseCollectionViewCell.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/7/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    func configureCell<T>(anyItem: T) {
        
    }
}

@objc protocol CollectionViewCellDelegate: class {
    @objc optional func seleted(indexPath: IndexPath)
}
