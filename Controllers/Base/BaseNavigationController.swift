//
//  BaseNavigationController.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/2/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.barTintColor = UIColor(hex: ColorApp.black)
        navigationBar.tintColor = UIColor(hex: ColorApp.white)
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(hex: ColorApp.white)]
    }

}
