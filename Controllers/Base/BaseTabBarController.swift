//
//  BaseTabBarController.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/2/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.unselectedItemTintColor = UIColor(hex: ColorApp.lightGray)
        tabBar.tintColor = UIColor(hex: ColorApp.orange)
        tabBar.barTintColor = UIColor(hex: ColorApp.black)
    }

}
