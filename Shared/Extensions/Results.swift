//
//  Results.swift
//  EnglishDictionary
//
//  Created by Kobe Pham on 2/3/17.
//  Copyright © 2017 Hung. All rights reserved.
//

import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
    
    func toList<T>(ofType: T.Type) -> List<T> {
        var array = List<T>()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}
