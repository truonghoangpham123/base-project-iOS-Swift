//
//  UITableView.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/8/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCellNib(anyClass: AnyClass) {
        let identifier = String.className(aClass: anyClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        register(nib, forCellReuseIdentifier: identifier)
    }

    func reloadData(with animation: UITableViewRowAnimation) {
        reloadSections(IndexSet(integersIn: 0..<numberOfSections), with: animation)
    }
    
}
