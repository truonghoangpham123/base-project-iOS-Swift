//
//  UICollectionView.swift
//  VNRecipes
//
//  Created by Kobe Pham on 1/8/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func registerCellNib(anyClass: AnyClass) {
        let identifier = String.className(aClass: anyClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        register(nib, forCellWithReuseIdentifier: identifier)
    }
}
