//
//  Utils.swift
//  VNRecipes
//
//  Created by Hung Thai on 1/10/17.
//  Copyright © 2018 Kobe Pham. All rights reserved.
//

import Foundation

class Utils {
   static func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    static func checkConnection() -> Bool {
        return Reachability.isConnectedToNetwork()
    }
    
    static func checkValidWith(regex: String, in text: String) -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: text)
    }
    
    static func getPathFileDocuments() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dataPath = documentsDirectory.appendingPathComponent("MP3Files")
        if FileManager.default.fileExists(atPath: dataPath.path) == false {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
        }
        return dataPath
    }
    
    
    static func getFileURL(url: String) -> URL? {
        let documentPath = getPathFileDocuments()
        let filePath = (documentPath.path as NSString).appendingPathComponent(Utils.getFileNameFromURL(url: url))
        guard let fileURL = URL(string: "file://\(filePath)") else {
            return nil
        }
        
        return fileURL
    }
    
    static func getFileURLLocal(fileName: String) -> URL? {
        let documentPath = getPathFileDocuments()
        let filePath = (documentPath.path as NSString).appendingPathComponent(fileName)
        
        guard let fileURL = URL(string: "file://\(filePath)") else {
            return nil
        }
        
        return fileURL
    }
    
    static func getFileNameFromURL(url: String) -> String {
        let urlComponent = url as NSString
        
        //url should be of type URL
        let audioFileName = String(urlComponent.lastPathComponent)
        
        //        //path extension will consist of the type of file it is, m4a or mp4
        //        let pathExtension = audioFileName.pathExtension
        
        return audioFileName!
    }
    
    static func checkFileExist(fileName: String = "", urlString: String) -> Bool {
        let documentPath = getPathFileDocuments()
        var filePath = ""
        if fileName == "" {
            filePath = (documentPath.path as NSString).appendingPathComponent(Utils.getFileNameFromURL(url: urlString))
        }
        else {
            filePath = (documentPath.path as NSString).appendingPathComponent(fileName)
        }
        return FileManager.default.fileExists(atPath: filePath)
    }
}
